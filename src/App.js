//Import React
import react from 'react';
import React from 'react';

//Import JSX
import NavBar from './Components/NavBar';
import Header from './Components/Header';
import Body from './Components/Body';
import Footer from './Components/Footer';

class App extends react.Component {

  constructor (props) {
    super(props)
  }

  render() {
    return(
        <div className='App'> 
            <NavBar/>
            <Header/>
            <Body/>
            <Footer/>
        </div>
    )
    }
}

export default App;